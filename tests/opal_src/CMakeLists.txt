add_subdirectory (BasicActions)
add_subdirectory (Distribution)
add_subdirectory (Elements)
add_subdirectory (Sample)
add_subdirectory (Utilities)

set (TEST_SRCS_LOCAL ${TEST_SRCS_LOCAL} PARENT_SCOPE)

# vi: set et ts=4 sw=4 sts=4:
  
# Local Variables:
# mode: cmake
# cmake-tab-width: 4
# indent-tabs-mode: nil
# require-final-newline: nil
# End:
