set (_SRCS
    DipoleFieldTest.cpp
    MultipoleTTest.cpp
    OffsetTest.cpp
    PolynomialTest.cpp
    RingTest.cpp
    SBend3DTest.cpp
    ScalingFFAMagnetTest.cpp
    TrimCoilTest.cpp
    VariableRFCavityTest.cpp
    VariableRFCavityFringeFieldTest.cpp
    VerticalFFAMagnetTest.cpp
)

include_directories (
  ${CMAKE_CURRENT_SOURCE_DIR}
)

add_sources(${_SRCS})

# vi: set et ts=4 sw=4 sts=4:
  
# Local Variables:
# mode: cmake
# cmake-tab-width: 4
# indent-tabs-mode: nil
# require-final-newline: nil
# End:
